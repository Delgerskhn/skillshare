﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Notification.Service.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notification.Service.Controllers
{
    public interface INotificationController
    {
        public IActionResult GetNotifications();
    }


    [ApiController]
    [Route("[controller]")]
    public class NotificationController : ControllerBase, INotificationController
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<NotificationController> _logger;

        public NotificationController(ILogger<NotificationController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetNotifications()
        {
            throw new NotImplementedException();
        }
    }
}
