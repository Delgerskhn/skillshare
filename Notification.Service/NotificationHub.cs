﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Notification.Service
{
    public class NotificationHub : Hub
    {
        public async Task Send(string userId, Entities.Notification notification)
        {
            // Call the broadcastMessage method to update clients.
            await Clients.All.SendAsync("broadcastMessage", userId, notification);
        }
    }
}