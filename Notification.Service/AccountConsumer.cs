
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Notification.Service.Data;
using Shared.Events.Commands;

namespace Notification.Service
{

    public interface INotificationSender
    {
        public Task sendNotification(int receiverId, Entities.Notification notification);
    }

    public class NotificationConsumer :
            IConsumer<CreateNotificationCommand>, INotificationSender
    {
        ILogger<NotificationConsumer> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IHubContext<NotificationHub> _hubContext;


        public NotificationConsumer(ILogger<NotificationConsumer> logger, ApplicationDbContext context, IHubContext<NotificationHub> hubContext)
        {
            _logger = logger;
            _context = context;
            _hubContext = hubContext;
        }

        public async Task Consume(ConsumeContext<CreateNotificationCommand> context)
        {
            //if user is currently active send notification with notificationhub
            //else save it in db
            _logger.LogInformation("Value: {Value}", context.Message);
            var account = JsonConvert.DeserializeObject<Entities.Notification>(JsonConvert.SerializeObject(context.Message));
            await _context.Notifications.AddAsync(account);
            await _context.SaveChangesAsync();
        }

        public async Task sendNotification(int receiverId, Entities.Notification notification)
        {
            //await _hubContext.Clients.Client(receiverId).SendAsync("receive_notification",notification);
            throw new System.NotImplementedException();
        }
    }
}