using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Notification.Service.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.Service.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Entities.Notification> Notifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // Get DbString from docker env
            // var connStr = Configuration["DbString"] ?? Configuration.GetConnectionString("Database");
            // options.UseNpgsql(connStr, x => x.MigrationsAssembly("Migrations"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Entities.Notification>().Property(r => r.NotificationType).HasColumnType("varchar");


            base.OnModelCreating(builder);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            var utcNow = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                if (entry.Entity is BaseEntity trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            // set the updated date to "now"
                            trackable.Updated = utcNow;

                            // mark property as "don't touch"
                            // we don't want to update on a Modify operation
                            entry.Property("Created").IsModified = false;
                            break;

                        case EntityState.Added:
                            // set both updated and created date to "now"
                            trackable.Created = utcNow;
                            trackable.Updated = utcNow;
                            break;
                    }
                }
            }
        }
    }
}