﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notification.Service.Entities
{
    public enum NotificationType
    {
        Follower, Blog, Tag
    }
    public class Notification:BaseEntity
    {
        public NotificationType NotificationType { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public int ReceiverId { get; set; }

    }
}
