﻿using System.Collections.Generic;

namespace Publication.Service.Entities
{
    public class Publisher:BaseEntity
    {
        public string Name { get; set; }
        public int OwnerId { get; set; }
        public Account Owner { get; set; }
        public List<PublicationWriter> PublicationWriters { get;  set; }
    }
}