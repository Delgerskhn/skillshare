﻿namespace Publication.Service.Entities
{
    public class FollowingWriter : BaseEntity
    {
        public int WriterId { get; set; }
        public int FollowerId { get; set; }

    }
}
