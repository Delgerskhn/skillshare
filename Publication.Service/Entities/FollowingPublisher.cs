﻿namespace Publication.Service.Entities
{
    public class FollowingPublisher : BaseEntity
    {
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }
        public Account Follower { get; set; }
        public int FollowerId { get; set; }

    }
}
