﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Publication.Service.Entities
{
    public class PublicationWriter:BaseEntity
    {
        public int PublisherId { get; set; }
        public int WriterId { get; set; }
        public Publisher Publisher { get; set; }
        public Account Account { get; set; }

    }
}
