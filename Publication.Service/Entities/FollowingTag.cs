﻿namespace Publication.Service.Entities
{
    public class FollowingTag : BaseEntity
    {
        public int TagId { get; set; }
        public int AccountId { get; set; }

    }
}
