using Publication.Service.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Publication.Service.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // Get DbString from docker env
            // var connStr = Configuration["DbString"] ?? Configuration.GetConnectionString("Database");
            // options.UseNpgsql(connStr, x => x.MigrationsAssembly("Migrations"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // builder
            //     .buildAccount()
            //     .buildOrder()
            //     .buildTransaction()
            //     .buildProduct();
            builder.Entity<PublicationWriter>().HasOne(r => r.Account).WithMany(r => r.PublicationWriters).HasForeignKey(r => r.WriterId);
            builder.Entity<PublicationWriter>().HasOne(r => r.Publisher).WithMany(r => r.PublicationWriters).HasForeignKey(r => r.PublisherId);
            builder.Entity<FollowingPublisher>().HasOne(r => r.Follower).WithMany(r => r.FollowingPublishers).HasForeignKey(r => r.FollowerId);


            base.OnModelCreating(builder);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            var utcNow = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                if (entry.Entity is BaseEntity trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            // set the updated date to "now"
                            trackable.Updated = utcNow;

                            // mark property as "don't touch"
                            // we don't want to update on a Modify operation
                            entry.Property("Created").IsModified = false;
                            break;

                        case EntityState.Added:
                            // set both updated and created date to "now"
                            trackable.Created = utcNow;
                            trackable.Updated = utcNow;
                            break;
                    }
                }
            }
        }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<FollowingPublisher> FollowingPublishers { get; set; }
        public DbSet<PublicationWriter> PublicationWriters { set; get; }
    }
}