﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Publication.Service.Migrations
{
    public partial class owner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "Publishers",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Publishers_OwnerId",
                table: "Publishers",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Publishers_Accounts_OwnerId",
                table: "Publishers",
                column: "OwnerId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Publishers_Accounts_OwnerId",
                table: "Publishers");

            migrationBuilder.DropIndex(
                name: "IX_Publishers_OwnerId",
                table: "Publishers");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Publishers");
        }
    }
}
