
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Publication.Service.Data;
using Publication.Service.Entities;
using Shared.Events.Commands;

namespace Publication.Service
{

    class AccountConsumer :
            IConsumer<CreateAccountCommand>
    {
        ILogger<AccountConsumer> _logger;
        private readonly ApplicationDbContext _context;

        public AccountConsumer(ILogger<AccountConsumer> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Consume(ConsumeContext<CreateAccountCommand> context)
        {
            _logger.LogInformation("Value: {Value}", context.Message);
            var account = JsonConvert.DeserializeObject<Account>(JsonConvert.SerializeObject(context.Message));
            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();
        }
    }
}