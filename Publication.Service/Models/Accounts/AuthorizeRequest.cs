
using Publication.Service.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Publication.Service.Models.Accounts
{
    public class AuthorizeRequest
    {
        public string token { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Role[] roles { get; set; }

    }
}