﻿using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Publication.Service.Data;
using Publication.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Publication.Service.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PublicationController : BaseController, IPublicationController
    {
        private readonly ApplicationDbContext _context;
        private readonly IPublishEndpoint _publishEndpoint;

        public PublicationController(ApplicationDbContext context, IPublishEndpoint publishEndpoint)
        {
            _context = context;
            _publishEndpoint = publishEndpoint;
        }

        [HttpPost]
        public IActionResult ApproveJoinRequest(int publisherId, int writerId)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public IActionResult Create(Publisher publisher)
        {
            throw new NotImplementedException();
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }
        [HttpGet]
        public IActionResult Get(int id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [Authorize(Role.Admin)]
        public IActionResult GetAll()
        {
            return Ok(_context.Publishers.ToList());
        }

        [HttpGet]
        [Authorize(Role.User, Role.Admin)]
        public IActionResult GetUserPublications()
        {
            return Ok(_context.Publishers.Where(r => r.OwnerId == Account.Id));
        }

        [HttpGet]
        [Authorize(Role.User, Role.Admin)]
        public IActionResult GetFollowingPublishers()
        {
            return Ok(_context.FollowingPublishers.Where(r => r.FollowerId == Account.Id).Select(r => r.Publisher).ToList());
        }

        [HttpGet]
        public IActionResult GetRecommended()
        {
            return Ok(_context.Publishers.OrderByDescending(r => r.PublicationWriters.Count).Take(5).ToList());
        }

        [HttpPost]
        public IActionResult InviteWriter(int writerId, int publisherId)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public IActionResult JoinPublication(int publisherId, int writerId)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public IActionResult Update(Publisher publisher)
        {
            throw new NotImplementedException();
        }

        public IActionResult FollowPublisher(int publisherId)
        {
            _context.FollowingPublishers.Add(new FollowingPublisher
            {
                PublisherId = publisherId,
                FollowerId = Account.Id,
            });
            return Ok();
        }
    }
}
