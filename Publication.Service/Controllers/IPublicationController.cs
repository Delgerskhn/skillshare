﻿using Microsoft.AspNetCore.Mvc;
using Publication.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Publication.Service.Controllers
{
    public interface IPublicationController
    {
        public IActionResult Create(Publisher publisher);
        public IActionResult Update(Publisher publisher);
        public IActionResult Delete(int id);

        public IActionResult InviteWriter(int writerId, int publisherId);
        public IActionResult JoinPublication(int publisherId, int writerId);
        public IActionResult GetUserPublications();
        public IActionResult Get(int id);
        IActionResult FollowPublisher(int publisherId);
        public IActionResult ApproveJoinRequest(int publisherId, int writerId);
    }
}
