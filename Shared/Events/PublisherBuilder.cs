using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace Shared.Events
{
    public static class PublisherBuilder
    {
        public static IServiceCollection AddPublisher(this IServiceCollection services)
        {

            services.AddMassTransit(x =>
                        {
                            x.UsingRabbitMq();
                        });

            services.AddMassTransitHostedService();
            return services;
        }

    }
}