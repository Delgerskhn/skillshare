using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace Shared.Events
{
    public static class ConsumerBuilder
    {
        public static IServiceCollection AddConsumer<T>(this IServiceCollection services, string EndPoint) where T : class, IConsumer
        {
            services.AddMassTransit(x =>
            {
                x.AddConsumer<T>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.ReceiveEndpoint(EndPoint, e =>
                    {
                        e.ConfigureConsumer<T>(context);
                    });
                });
            });

            services.AddMassTransitHostedService();
            return services;
        }
    }
}