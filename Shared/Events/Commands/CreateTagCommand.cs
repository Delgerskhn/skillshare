

using System;

namespace Shared.Events.Commands
{
    public class CreateTagCommand
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string Name { get; set; }

    }
}