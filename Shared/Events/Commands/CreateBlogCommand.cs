

using System;
using System.Collections.Generic;
using Shared.Data;

namespace Shared.Events.Commands
{
    public class CreateBlogCommand
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int UserId { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}