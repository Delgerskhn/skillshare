﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Shared.Data
{
    public partial class Blog : BaseEntity
    {
        public Blog()
        {
            Comments = new HashSet<Comment>();
            Tags = new HashSet<Tag>();
        }
        public int Likes { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Img { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }


        public virtual ICollection<Tag> Tags { get; set; }
        public virtual BlogStatus BlogStatus { get; set; }
        public virtual Account User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
