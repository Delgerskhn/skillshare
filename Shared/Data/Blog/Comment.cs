﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Shared.Data
{
    public partial class Comment : BaseEntity
    {
        public int? BlogId { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }

        public virtual Blog Blog { get; set; }
        public virtual Account User { get; set; }
    }
}
