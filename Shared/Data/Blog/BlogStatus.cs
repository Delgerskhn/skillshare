﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Shared.Data
{
    public enum BlogStatus
    {
        PENDING, APPROVED, DECLINED, REVIEWED, REMOVED
    }
}
