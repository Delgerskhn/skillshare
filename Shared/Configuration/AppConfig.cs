using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Shared.Configuration
{
    public static class AppConfig
    {
        public static Action<HostBuilderContext, IConfigurationBuilder> ConfigureDelegate = (hostingContext, config) =>
            {
                var env = hostingContext.HostingEnvironment;
                var sharedFolder = Path.Combine(env.ContentRootPath, "..", "Configs");
                config
                    .AddJsonFile(Path.Combine(sharedFolder, $"SharedSettings.{env.EnvironmentName}.json"), optional: true)
                    .AddJsonFile("appsettings.json", optional: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
                config.AddEnvironmentVariables();

            };

    }
}