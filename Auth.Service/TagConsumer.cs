
using System.Threading.Tasks;
using Auth.Service.Data;
using Auth.Service.Entities;
using MassTransit;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Shared.Events.Commands;

namespace WebApi
{

    class TagConsumer :
            IConsumer<CreateTagCommand>
    {
        ILogger<TagConsumer> _logger;
        private readonly ApplicationDbContext _context;

        public TagConsumer(ILogger<TagConsumer> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Consume(ConsumeContext<CreateTagCommand> context)
        {
            _logger.LogInformation("Value: {Value}", context.Message);
            var command = JsonConvert.DeserializeObject<Tag>(JsonConvert.SerializeObject(context.Message));
            var acc = await _context.Tags.AddAsync(command);
            await _context.SaveChangesAsync();
        }
    }
}