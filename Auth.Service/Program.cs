﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                // .ConfigureAppConfiguration(
                //     (hostingContext, config) =>
                //     {
                //         var env = hostingContext.HostingEnvironment;
                //         config
                //             .AddJsonFile("appsettings.json", optional: true)
                //             .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
                //         config.AddEnvironmentVariables();

                //     }
                // )
                .ConfigureWebHostDefaults(x =>
                {
                    x.UseStartup<Startup>();
                });
        }
    }
}
