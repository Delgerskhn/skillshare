﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Auth.Service.Migrations
{
    public partial class usersettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FollowingPublishers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    FollowerId = table.Column<int>(type: "integer", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowingPublishers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FollowingTags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TagId = table.Column<int>(type: "integer", nullable: false),
                    AccountId = table.Column<int>(type: "integer", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowingTags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FollowingWriters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WriterId = table.Column<int>(type: "integer", nullable: false),
                    FollowerId = table.Column<int>(type: "integer", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowingWriters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SavedBlogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountId = table.Column<int>(type: "integer", nullable: false),
                    BlogId = table.Column<int>(type: "integer", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Updated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedBlogs", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FollowingPublishers_FollowerId_PublisherId",
                table: "FollowingPublishers",
                columns: new[] { "FollowerId", "PublisherId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FollowingTags_AccountId_TagId",
                table: "FollowingTags",
                columns: new[] { "AccountId", "TagId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FollowingWriters_FollowerId_WriterId",
                table: "FollowingWriters",
                columns: new[] { "FollowerId", "WriterId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SavedBlogs_BlogId_AccountId",
                table: "SavedBlogs",
                columns: new[] { "BlogId", "AccountId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FollowingPublishers");

            migrationBuilder.DropTable(
                name: "FollowingTags");

            migrationBuilder.DropTable(
                name: "FollowingWriters");

            migrationBuilder.DropTable(
                name: "SavedBlogs");
        }
    }
}
