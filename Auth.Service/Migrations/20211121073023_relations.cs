﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Auth.Service.Migrations
{
    public partial class relations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_SavedBlogs_AccountId",
                table: "SavedBlogs",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowingWriters_WriterId",
                table: "FollowingWriters",
                column: "WriterId");

            migrationBuilder.AddForeignKey(
                name: "FK_FollowingPublishers_Accounts_FollowerId",
                table: "FollowingPublishers",
                column: "FollowerId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FollowingTags_Accounts_AccountId",
                table: "FollowingTags",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FollowingWriters_Accounts_FollowerId",
                table: "FollowingWriters",
                column: "FollowerId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FollowingWriters_Accounts_WriterId",
                table: "FollowingWriters",
                column: "WriterId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SavedBlogs_Accounts_AccountId",
                table: "SavedBlogs",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FollowingPublishers_Accounts_FollowerId",
                table: "FollowingPublishers");

            migrationBuilder.DropForeignKey(
                name: "FK_FollowingTags_Accounts_AccountId",
                table: "FollowingTags");

            migrationBuilder.DropForeignKey(
                name: "FK_FollowingWriters_Accounts_FollowerId",
                table: "FollowingWriters");

            migrationBuilder.DropForeignKey(
                name: "FK_FollowingWriters_Accounts_WriterId",
                table: "FollowingWriters");

            migrationBuilder.DropForeignKey(
                name: "FK_SavedBlogs_Accounts_AccountId",
                table: "SavedBlogs");

            migrationBuilder.DropIndex(
                name: "IX_SavedBlogs_AccountId",
                table: "SavedBlogs");

            migrationBuilder.DropIndex(
                name: "IX_FollowingWriters_WriterId",
                table: "FollowingWriters");
        }
    }
}
