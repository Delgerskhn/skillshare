using Auth.Service.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Auth.Service.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // Get DbString from docker env
            // var connStr = Configuration["DbString"] ?? Configuration.GetConnectionString("Database");
            // options.UseNpgsql(connStr, x => x.MigrationsAssembly("Migrations"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // builder
            //     .buildAccount()
            //     .buildOrder()
            //     .buildTransaction()
            //     .buildProduct();

            builder.Entity<FollowingTag>().HasOne(r => r.Account).WithMany(r => r.FollowingTags).HasForeignKey(r => r.AccountId);
            builder.Entity<FollowingWriter>().HasOne(r => r.Follower).WithMany(r => r.FollowingWriters).HasForeignKey(r => r.FollowerId);
            builder.Entity<FollowingWriter>().HasOne(r => r.Writer).WithMany(r => r.Followers).HasForeignKey(r => r.WriterId);
            builder.Entity<FollowingPublisher>().HasOne(r => r.Follower).WithMany(r => r.FollowingPublishers).HasForeignKey(r => r.FollowerId);
            builder.Entity<SavedBlog>().HasOne(r => r.Account).WithMany(r => r.SavedBlogs).HasForeignKey(r => r.AccountId);


            builder.Entity<FollowingTag>().HasIndex(r => new { r.AccountId, r.TagId }).IsUnique();
            builder.Entity<FollowingWriter>().HasIndex(r => new { r.FollowerId, r.WriterId }).IsUnique();
            builder.Entity<FollowingPublisher>().HasIndex(r => new { r.FollowerId, r.PublisherId }).IsUnique();
            builder.Entity<SavedBlog>().HasIndex(r => new { r.BlogId, r.AccountId }).IsUnique();

            base.OnModelCreating(builder);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            var utcNow = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                if (entry.Entity is BaseEntity trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            // set the updated date to "now"
                            trackable.Updated = utcNow;

                            // mark property as "don't touch"
                            // we don't want to update on a Modify operation
                            entry.Property("CreatedOn").IsModified = false;
                            break;

                        case EntityState.Added:
                            // set both updated and created date to "now"
                            trackable.Created = utcNow;
                            trackable.Updated = utcNow;
                            break;
                    }
                }
            }
        }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<FollowingTag> FollowingTags { get; set; }
        public DbSet<FollowingWriter> FollowingWriters { get; set; }
        public DbSet<FollowingPublisher> FollowingPublishers { get; set; }
        public DbSet<SavedBlog> SavedBlogs { get; set; }
        public DbSet<Tag> Tags { get; set; }


    }
}