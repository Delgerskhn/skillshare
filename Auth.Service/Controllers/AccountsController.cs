﻿using Auth.Service.Data;
using Auth.Service.Entities;
using Auth.Service.Models.Accounts;
using Auth.Service.Services;
using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared.Events.Commands;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Helpers;
using WebApi.Models.Accounts;

namespace WebApi.Controllers
{
    public interface IAccountsController
    {
        ActionResult<AuthenticateResponse> Authenticate(AuthenticateRequest model);
        IActionResult Delete(int id);
        IActionResult ForgotPassword(ForgotPasswordRequest model);
        ActionResult<AccountResponse> GetById(int id);
        Task<IActionResult> Register(RegisterRequest model);
        IActionResult ResetPassword(ResetPasswordRequest model);
        ActionResult<AccountResponse> Update(int id, UpdateRequest model);
        IActionResult FollowWriter(int writerId);
        IActionResult FollowTag(int tagId);
        IActionResult GetFollowingTags(int tagId);
        IActionResult SaveBlog(int blogId);

    }

    [ApiController]
    [Route("[controller]")]
    public class AccountsController : BaseController, IAccountsController
    {
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ApplicationDbContext _context;
        private readonly IPublishEndpoint _publishEndpoint;

        public AccountsController(
            IAccountService accountService,
            IOptions<AppSettings> appSettings,
            IMapper mapper, ApplicationDbContext context, IPublishEndpoint publishEndpoint)
        {
            _appSettings = appSettings.Value;
            _accountService = accountService;
            _mapper = mapper;
            _context = context;
            _publishEndpoint = publishEndpoint;
        }

        [HttpPost("authenticate")]
        public ActionResult<AuthenticateResponse> Authenticate(AuthenticateRequest model)
        {
            var response = _accountService.Authenticate(model, ipAddress());
            setTokenCookie(response.RefreshToken);
            return Ok(response);
        }

        [HttpPost("authorize")]
        public async Task<ActionResult<Account>> Authorize(AuthorizeRequest req)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            tokenHandler.ValidateToken(req.token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var accountId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
            var account = await _context.Accounts.FindAsync(accountId);
            if (account == null || (req.roles.Any() && !req.roles.Contains(account.Role)))
                return Unauthorized(new { message = "Unauthorized" });
            return Ok(account);
        }

        [HttpPost("refresh-token")]
        public ActionResult<AuthenticateResponse> RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var response = _accountService.RefreshToken(refreshToken, ipAddress());
            setTokenCookie(response.RefreshToken);
            return Ok(response);
        }

        [Authorize]
        [HttpPost("revoke-token")]
        public IActionResult RevokeToken(RevokeTokenRequest model)
        {
            // accept token from request body or cookie
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return BadRequest(new { message = "Token is required" });

            // users can revoke their own tokens and admins can revoke any tokens
            if (!Account.OwnsToken(token) && Account.Role != Role.Admin)
                return Unauthorized(new { message = "Unauthorized" });

            _accountService.RevokeToken(token, ipAddress());
            return Ok(new { message = "Token revoked" });
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterRequest model)
        {
            var acc = _accountService.Register(model, Request.Headers["origin"]);
            await _publishEndpoint.Publish<CreateAccountCommand>(acc);
            return Ok(new { message = "Registration successful, please check your email for verification instructions" });
        }

        [HttpPost("verify-email")]
        public IActionResult VerifyEmail(VerifyEmailRequest model)
        {
            _accountService.VerifyEmail(model.Token);
            return Ok(new { message = "Verification successful, you can now login" });
        }

        [HttpPost("forgot-password")]
        public IActionResult ForgotPassword(ForgotPasswordRequest model)
        {
            _accountService.ForgotPassword(model, Request.Headers["origin"]);
            return Ok(new { message = "Please check your email for password reset instructions" });
        }

        [HttpPost("validate-reset-token")]
        public IActionResult ValidateResetToken(ValidateResetTokenRequest model)
        {
            _accountService.ValidateResetToken(model);
            return Ok(new { message = "Token is valid" });
        }

        [HttpPost("reset-password")]
        public IActionResult ResetPassword(ResetPasswordRequest model)
        {
            _accountService.ResetPassword(model);
            return Ok(new { message = "Password reset successful, you can now login" });
        }

        [Authorize(Role.Admin)]
        [HttpGet]
        public ActionResult<IEnumerable<AccountResponse>> GetAll()
        {
            var accounts = _accountService.GetAll();
            return Ok(accounts);
        }

        [HttpGet("authors")]
        public async Task<ActionResult<ICollection<Account>>> Authors()
        {
            var q = await _context.Accounts.Where(r => r.Role == Role.User).ToListAsync();
            return q;
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public ActionResult<AccountResponse> GetById(int id)
        {
            // users can get their own account and admins can get any account
            if (id != Account.Id && Account.Role != Role.Admin)
                return Unauthorized(new { message = "Unauthorized" });

            var account = _accountService.GetById(id);
            return Ok(account);
        }

        [Authorize(Role.Admin)]
        [HttpPost]
        public ActionResult<AccountResponse> Create(CreateRequest model)
        {
            var account = _accountService.Create(model);
            return Ok(account);
        }

        [Authorize]
        [HttpPut("{id:int}")]
        public ActionResult<AccountResponse> Update(int id, UpdateRequest model)
        {
            // users can update their own account and admins can update any account
            if (id != Account.Id && Account.Role != Role.Admin)
                return Unauthorized(new { message = "Unauthorized" });

            // only admins can update role
            if (Account.Role != Role.Admin)
                model.Role = null;

            var account = _accountService.Update(id, model);
            return Ok(account);
        }

        [Authorize]
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            // users can delete their own account and admins can delete any account
            if (id != Account.Id && Account.Role != Role.Admin)
                return Unauthorized(new { message = "Unauthorized" });

            _accountService.Delete(id);
            return Ok(new { message = "Account deleted successfully" });
        }

        // helper methods

        private void setTokenCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(_appSettings.RefreshTokenExpireDays)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }

        private string ipAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        public IActionResult FollowWriter(int writerId)
        {
            throw new NotImplementedException();
        }

        [HttpPost("followtag/{tagId}")]
        public IActionResult FollowTag(int tagId)
        {
            var old = _context.FollowingTags.SingleOrDefault(r => r.AccountId == Account.Id && r.TagId == tagId);
            if (old == null)
            {
                _context.FollowingTags.Add(new FollowingTag
                {
                    AccountId = Account.Id,
                    TagId = tagId
                });
            }
            else
            {
                _context.Remove(old);
            }
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("saveblog/{blogId}")]
        public IActionResult SaveBlog(int blogId)
        {
            _context.SavedBlogs.Add(new SavedBlog
            {
                BlogId = blogId,
                AccountId = Account.Id
            });
            _context.SaveChanges();
            return Ok();
        }

        [Authorize(Role.User, Role.Admin)]
        [HttpGet("GetFollowingTags")]
        public IActionResult GetFollowingTags(int tagId)
        {
            return Ok(_context.FollowingTags.Where(r => r.AccountId == Account.Id).Select(r => r.Tag).ToList());
            throw new NotImplementedException();
        }

        [HttpGet("getrecommendedwriters")]
        public IActionResult GetRecommWriters()
        {
            return Ok(_context.Accounts.OrderByDescending(r => r.Followers.Count).ThenByDescending(r => r.PublishedBlogs).Take(10));
        }

        [HttpGet("getsavedblogs")]
        [Authorize(Role.User, Role.Admin)]
        public IActionResult GetSavedblogs()
        {
            return Ok(_context.SavedBlogs.Where(r => r.AccountId == Account.Id).Select(r => r.BlogId).ToList());
        }
    }
}
