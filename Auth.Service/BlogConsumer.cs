
using System.Threading.Tasks;
using Auth.Service.Data;
using Auth.Service.Entities;
using MassTransit;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Shared.Events.Commands;

namespace WebApi
{

    class BlogConsumer :
            IConsumer<CreateBlogCommand>
    {
        ILogger<BlogConsumer> _logger;
        private readonly ApplicationDbContext _context;

        public BlogConsumer(ILogger<BlogConsumer> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Consume(ConsumeContext<CreateBlogCommand> context)
        {
            _logger.LogInformation("Value: {Value}", context.Message);
            var command = JsonConvert.DeserializeObject<CreateBlogCommand>(JsonConvert.SerializeObject(context.Message));
            var acc = await _context.Accounts.FindAsync(command.UserId);
            if (acc != null)
            {
                acc.PublishedBlogs++;
            }
            await _context.SaveChangesAsync();
        }
    }
}