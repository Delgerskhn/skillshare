using System;
using System.Collections.Generic;

namespace Auth.Service.Entities
{
    public class Account : BaseEntity
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public int PublishedBlogs { get; set; }

        public string ImgUrl { get; set; }


        public bool AcceptTerms { get; set; }
        public Role Role { get; set; }
        public string VerificationToken { get; set; }
        public DateTime? Verified { get; set; }
        public bool IsVerified => Verified.HasValue || PasswordReset.HasValue;
        public string ResetToken { get; set; }
        public DateTime? ResetTokenExpires { get; set; }
        public DateTime? PasswordReset { get; set; }
        public List<RefreshToken> RefreshTokens { get; set; }
        public List<FollowingTag> FollowingTags { get; set; }
        public List<FollowingWriter> FollowingWriters { get; set; }
        public List<FollowingWriter> Followers { get; set; }
        public List<FollowingPublisher> FollowingPublishers { get; set; }
        public List<SavedBlog> SavedBlogs { get; set; }

        public bool OwnsToken(string token)
        {
            return this.RefreshTokens?.Find(x => x.Token == token) != null;
        }
    }
}