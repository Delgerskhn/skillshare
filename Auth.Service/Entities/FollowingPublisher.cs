﻿namespace Auth.Service.Entities
{
    public class FollowingPublisher:BaseEntity
    {
        public int PublisherId { get; set; }
        public int FollowerId { get; set; }
        public Account Follower { get;  set; }
    }
}
