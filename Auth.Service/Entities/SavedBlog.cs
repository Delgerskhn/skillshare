﻿namespace Auth.Service.Entities
{
    public class SavedBlog:BaseEntity
    {
        public int AccountId { get; set; }
        public int BlogId { get; set; }
        public Account Account { get; internal set; }
    }
}
