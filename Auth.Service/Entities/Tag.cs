﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

#nullable disable

namespace Auth.Service.Entities
{
    public partial class Tag : BaseEntity
    {
        public string Name { get; set; }

    }
}
