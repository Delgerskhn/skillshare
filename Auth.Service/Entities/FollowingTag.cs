﻿namespace Auth.Service.Entities
{
    public class FollowingTag : BaseEntity
    {
        public int TagId { get; set; }
        public Tag Tag { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

    }
}
