﻿using System.Collections.Generic;

namespace Auth.Service.Entities
{
    public class StaffPermission:BaseEntity
    {
        public int StaffId { get; set; }
        public Account Staff { get; set; }
        public List<Tag> ManagingTags { get; set; }
    }
}
