﻿namespace Auth.Service.Entities
{
    public class FollowingWriter:BaseEntity
    {
        public int WriterId { get; set; }
        public int FollowerId { get; set; }
        public Account Follower { get; set; }
        public Account Writer { get;  set; }
    }
}
