
using Auth.Service.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Auth.Service.Models.Accounts
{
    public class AuthorizeRequest
    {
        public string token { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Role[] roles { get; set; }

    }
}