import { Container, Grid, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { SearchInput } from "../components/forms/search-input";
import { Overview } from "../components/publication/overview";

const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
}));
export default function Publication() {
  const classes = useStyles();
  const searchPublication = () => {};
  return (
    <main>
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            Publications
          </Typography>
          <SearchInput onSubmitCallback={searchPublication} />
        </Container>
      </div>
      <Grid container spacing={4}>
        <Overview />
      </Grid>
    </main>
  );
}
