import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { useRouter } from "next/router";
import { constBlog } from "../../shared/constants";
import { getBlogsByStatus } from "../../api/blogs";
import { useAppContext } from "../../context/app";
import Profile from "../../components/account/profile";
import { useAuth } from "../../context/auth";
import { Post } from "../../components/blogs/post";
import { withAuth } from "../../shared/with-auth";
import { getFollowingTags, withdrawSalary } from "../../api/account";
import { mapStatus } from "../../helpers/BlogStatusMapper";
import { Chip, Container } from "@material-ui/core";
import BackgroundLetterAvatars from "../../components/account/avatar";
import { PublicationProfile } from "../../components/account/publication.profile";
import { PublicationsContainer } from "../../components/container/recommended.publications";
import { GetFollowingPubs, GetUserPubs } from "../../api/publication";
import { TagsContainer } from "../../components/container/TagsContainer";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));
function Dashboard() {
  const classes = useStyles();
  const router = useRouter();
  const [status, setStatus] = React.useState(0);
  const [posts, setPosts] = React.useState([]);
  const { setIsLoading, setSuccessMsg } = useAppContext();
  const { user } = useAuth();

  useEffect(() => {
    const { query } = router;
    query.status = parseInt(query.status);
    if (Number.isInteger(query.status)) {
      handleChange(null, query.status - 1);
    } else handleChange(null, 0);
    return () => setIsLoading(false);
  }, [router.query]);

  const fetchPosts = async (status) => {
    setIsLoading(true);
    var res = await getBlogsByStatus(mapStatus(status));
    setIsLoading(false);
    setPosts(res);
  };

  const handleChange = (event, newValue) => {
    fetchPosts(newValue);
    setStatus(newValue);
  };

  const onWithdraw = async () => {
    setIsLoading(true);
    let res = await withdrawSalary();
    setSuccessMsg("Withdraw successful!");
    setIsLoading(false);
  };

  return (
    <div className={classes.root}>
      <Container maxWidth="lg">
        <Grid container spacing={12}>
          <Grid item container spacing={4} md={8}>
            <Grid item md={12}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <Tabs
                  value={status}
                  onChange={handleChange}
                  indicatorColor="primary"
                  textColor="secondary"
                >
                  <Tab label="Pending" />
                  <Tab label="Published" />
                  <Tab label="Declined" />
                  <Tab label="Draft" />
                </Tabs>
              </Box>
            </Grid>
            {posts.map((post) => (
              <Post key={post.title} post={post} />
            ))}
          </Grid>
          <Grid item container md={4}>
            <Grid md={12} item>
              <Box display="flex" flexDirection="column">
                <Typography mt={2} variant="subtitle1" color="textSecondary">
                  PROFILE
                </Typography>
                <Box
                  display="flex"
                  m={2}
                  flexDirection="column"
                  alignItems="center"
                >
                  <BackgroundLetterAvatars
                    user={{ firstName: "Delger" }}
                    sx={{ width: 60, height: 60 }}
                    src=""
                  />
                  <Typography variant="subtitle1" color="textSecondary">
                    Delgersaikhan B.
                  </Typography>
                </Box>

                <PublicationsContainer
                  getDataSource={GetUserPubs}
                  title="YOUR PUBLICATIONS"
                />

                <PublicationsContainer
                  getDataSource={GetFollowingPubs}
                  title="FOLLOWING PUBLICATIONS"
                />

                <TagsContainer
                  title="FOLLOWING TAGS"
                  getDataSource={getFollowingTags}
                />
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Container>
      {/* <Grid mt={3} container spacing={4}>
        <Grid item xs={12}>
          <Profile
            onClick={onWithdraw}
            user={user}
            reputation={user?.withdrawReputation}
          />
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.root}>
            <Tabs
              value={status}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              centered
            >
              <Tab label="Pending" />
              <Tab label="Published" />
              <Tab label="Declined" />
              <Tab label="Draft" />
            </Tabs>
          </Paper>
        </Grid>
        {posts.map((post) => (
          <Post key={post.id} post={post} hasController />
        ))}
      </Grid> */}
    </div>
  );
}

export default withAuth(Dashboard);
