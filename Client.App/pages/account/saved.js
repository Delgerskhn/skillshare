import { Container, Grid } from "@material-ui/core";
import { useRouter } from "next/router";
import { getSavedPosts } from "../../api/account";
import { PostsContainer } from "../../components/container/posts.container";
import { HomeSidebar } from "../../components/home/home.sidebar";
import { withAuth } from "../../shared/with-auth";

const Saved = () => {
  const router = useRouter();
  const searchContent = (val) => {
    router.push("/search?q=" + val);
  };
  return (
    <main>
      <Container maxWidth="lg">
        <Grid container spacing={12}>
          <Grid item container spacing={4} md={8}>
            <PostsContainer fetchPosts={getSavedPosts} />
          </Grid>
          <HomeSidebar searchContent={searchContent} />
        </Grid>
      </Container>
    </main>
  );
};

export default withAuth(Saved);
