import React, { useEffect } from "react";
import { useRouter } from "next/router";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import GitHubIcon from "@material-ui/icons/GitHub";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import Sidebar from "../shared/sidebar";
import Fetch from "../helpers/fetch";
import { Avatar, Box, Container, Divider, Typography } from "@material-ui/core";
import BlogEditor from "../components/editor/blog-editor";
import { getBlog, writeComment } from "../api/blogs";
import Profile from "../components/account/profile";
import { useBlogContext } from "../context/blog";
import { useAuth } from "../context/auth";
import Comments from "../components/blogs/comments";
import { useAppContext } from "../context/app";

const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));

export default function Blog(props) {
  const classes = useStyles();
  const { setBlog, setComments, comments, blog } = useBlogContext();
  const router = useRouter();
  const { pk } = router.query;
  const { user } = useAuth();
  const { setIsLoading } = useAppContext();

  const sendComment = async (value) => {
    let comment = { content: value, userId: user?.id, blogId: blog.id };
    setIsLoading(true);
    let res = await writeComment(comment);
    setIsLoading(false);
    setComments([...comments, comment]);
  };

  const parseContent = (contentStr) => {
    try {
      return JSON.parse(contentStr);
    } catch {
      return null;
    }
  };

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      var res = await getBlog(pk);
      setIsLoading(false);
      setBlog(res);
      setComments(res?.comments || []);
    })();
  }, []);
  return (
    <main>
      <Container maxWidth="lg">
        <Grid container spacing={5} className={classes.mainGrid}>
          <Grid item xs={12} md={8}>
            <Profile user={blog?.user} reputation={blog?.appUser?.reputation} />

            <Divider />
            {blog && <BlogEditor content={parseContent(blog.content)} />}
          </Grid>

          <Sidebar />

          <Comments onSubmit={sendComment} comments={comments} />
        </Grid>
      </Container>
    </main>
  );
}
