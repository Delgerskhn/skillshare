import { Container, Grid } from "@material-ui/core";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { getBlogsByContent } from "../api/blogs";
import { PostsContainer } from "../components/container/posts.container";
import { HomeSidebar } from "../components/home/home.sidebar";
import { useAppContext } from "../context/app";
import { withAuth } from "../shared/with-auth";

const Search = () => {
  const router = useRouter();
  const { q } = router.query;
  const { setIsLoading, setErrorMsg } = useAppContext();

  const fetchPosts = async () => {
    if (!q) return;
    setIsLoading(true);
    let res = await getBlogsByContent(q);
    setIsLoading(false);
    return res;
  };

  const searchContent = (val) => {
    router.push("/search?q=" + val);
  };

  return (
    <main>
      <Container maxWidth="lg">
        <Grid container spacing={12}>
          <Grid item container spacing={4} md={8}>
            {q && <PostsContainer fetchPosts={fetchPosts} />}
          </Grid>
          <HomeSidebar searchContent={searchContent} />
        </Grid>
      </Container>
    </main>
  );
};

export default withAuth(Search);
