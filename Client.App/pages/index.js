import React, { useEffect, useState } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Fetch from "../helpers/fetch";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import { getBlogsByContent, getBlogsByTag, getLatestBlogs } from "../api/blogs";
import { Post } from "../components/blogs/post";
import { SearchInput } from "../components/forms/search-input";
import { useAppContext } from "../context/app";
import { HomeSidebar } from "../components/home/home.sidebar";
import { HomePosts } from "../components/home/home.posts";
import { useRouter } from "next/router";
const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
}));

export default function Index({ prePosts }) {
  const [posts, setPosts] = useState([]);
  const router = useRouter();
  const { tag } = router.query;
  const classes = useStyles();
  const { setIsLoading, setErrorMsg } = useAppContext();

  useEffect(() => {
    (async () => {
      var res = [];
      setIsLoading(true);
      if (tag) {
        res = await getBlogsByTag(tag);
        if (!res.length) throw "Not found";
      } else {
        res = await getLatestBlogs();
      }
      setIsLoading(false);
      setPosts(res);
    })();
  }, []);

  const searchContent = (val) => {
    router.push("/search?q=" + val);
  };
  return (
    <main>
      {/* <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            Watchu lookinfo?
          </Typography>
          <SearchInput onSubmitCallback={searchContent} />
        </Container>
      </div> */}
      <Container maxWidth="lg">
        <Grid container spacing={12}>
          <HomePosts posts={posts} />
          <HomeSidebar searchContent={searchContent} />
        </Grid>
      </Container>
    </main>
  );
}

Index.propTypes = {
  posts: PropTypes.array,
};
