import { Link, Typography } from "@material-ui/core";

export function Signature() {
  return (
    <Link href="/">
      <Typography
        component="h2"
        variant="h5"
        color="inherit"
        align="left"
        noWrap
        color="textSecondary"
      >
        Fiboshare
      </Typography>
    </Link>
  );
}
