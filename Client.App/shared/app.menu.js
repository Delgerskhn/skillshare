import React from "react";
import { Box, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import DashboardCustomizeIcon from "@material-ui/icons/DashboardCustomize";
import FavoriteIcon from "@material-ui/icons/Favorite";
import NotificationImportantIcon from "@material-ui/icons/NotificationImportant";
import HomeWorkIcon from "@material-ui/icons/HomeWork";
import CreateIcon from "@material-ui/icons/Create";
import { useRouter } from "next/router";

export const AppMenu = () => {
  const router = useRouter();

  return (
    <Box display="inline-flex" gap={2} alignItems="center">
      <Box
        onClick={() => router.push("/account/dashboard")}
        sx={{ boxShadow: 3, borderRadius: 1 }}
      >
        <IconButton aria-label="dashboard" size="small">
          <DashboardCustomizeIcon s />
        </IconButton>
      </Box>
      <Box
        onClick={() => router.push("/account/saved")}
        sx={{ boxShadow: 3, borderRadius: 1 }}
      >
        <IconButton aria-label="saved" size="small">
          <FavoriteIcon />
        </IconButton>
      </Box>
      <Box
        onClick={() => router.push("")}
        sx={{ boxShadow: 3, borderRadius: 1 }}
      >
        <IconButton aria-label="notifications" size="small">
          <NotificationImportantIcon />
        </IconButton>
      </Box>
      {/* <Box
        onClick={() => router.push("/publication")}
        
        sx={{ boxShadow: 3, borderRadius: 1 }}
      >
        <IconButton aria-label="publication" size="small">
          <HomeWorkIcon />
        </IconButton>
      </Box> */}
      <Box
        onClick={() => router.push("/editor")}
        sx={{ boxShadow: 3, borderRadius: 1 }}
      >
        <IconButton aria-label="write" size="small">
          <CreateIcon />
        </IconButton>
      </Box>
    </Box>
  );
};
