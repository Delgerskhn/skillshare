import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { useAppContext } from "../context/app";
import { AccountPopover } from "../components/popover";
import BorderColorIcon from "@material-ui/icons/BorderColor";
import { TagSearch } from "../components/forms/tag-search";
import TagSelect from "../components/forms/tag-select";
import { useAuth } from "../context/auth";
import AppProfile from "../components/account/AppProfile";
import { Box, Chip, Input, TextField } from "@material-ui/core";
import { AppMenu } from "./app.menu";
import { Signature } from "./signature";
import LoginIcon from "@material-ui/icons/Login";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    justifyContent: "space-between",
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: "space-between",
    overflowX: "auto",
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
  btn: {
    marginRight: theme.spacing(1),
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const { user } = useAuth();
  const router = useRouter();

  const { popularTags } = useAppContext();
  const { title } = props;
  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
        <Box display="inline-flex" gap={5} alignItems="center">
          <Signature />
          {/* <Input placeholder="Search.." /> */}
        </Box>
        <AppMenu />
        {/*  <IconButton>
                <SearchIcon />
              </IconButton>*/}
        <Box display="flex" alignItems="center">
          {!user ? (
            <Button
              onClick={() => router.push("/auth/login")}
              sx={{ boxShadow: 3, borderRadius: 1, paddingY: 0 }}
            >
              <IconButton aria-label="delete" size="small">
                <LoginIcon />
              </IconButton>
              <Typography color="gray" ml={1} mr={2} variant="button">
                Sign in
              </Typography>
            </Button>
          ) : (
            // <React.Fragment>
            //   <Link href="auth/login">
            //     <Button variant="outlined" size="small" className={classes.btn}>
            //       Sign in
            //     </Button>
            //   </Link>
            // </React.Fragment>
            //  <AccountPopover />
            <AppProfile />
          )}
        </Box>
      </Toolbar>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Box display="inline-flex" gap={2} alignItems="center">
          <Typography ml={3} variant="subtitle1" color="textSecondary">
            POPULAR TOPICS
          </Typography>
          {popularTags.slice(0, 9).map((tag) => (
            <Chip
              clickable
              label={tag.name}
              key={tag.pk}
              onClick={() => router.push("/?tag=" + tag.id)}
            />
          ))}
        </Box>
        {<TagSelect />}
      </Box>
      <Box mb={3} />
    </React.Fragment>
  );
}

Header.propTypes = {
  sections: PropTypes.array,
  title: PropTypes.string,
};
