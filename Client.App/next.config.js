module.exports = {
  serverRuntimeConfig: {
    // Will only be available on the server side
    URI: "http://gateway:5002",
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    URI: "http://localhost:5002",
  },
};
