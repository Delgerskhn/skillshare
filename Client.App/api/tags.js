import Fetch from "../helpers/fetch";

let tags = [];

const createTag = async (tag) => {
  tags.push(tag);
  try {
    return await Fetch("/blog/writers/tag", "post", tag, true);
  } catch {}
};

const fetchTags = async () => {
  try {
    tags = await Fetch("/blog/readers/tags", "get");
  } catch {
    tags = [];
  }
};

const getTags = () => tags;

export const getAllTags = async () => {
  return await Fetch("/blog/readers/tags", "get");
};

export const getTag = async (id) => {
  return await Fetch("/blog/readers/gettag/" + id, "get");
};

export const followTag = async (id) => {
  return await Fetch("/auth/accounts/followtag/" + id, "post", null, true);
};

export const getFollowingTags = async () => {
  return await Fetch("/auth/accounts/GetFollowingTags", "get", null, true);
};

export { getTags, createTag, fetchTags };
