import Fetch, { WrapResult } from "../helpers/fetch";

export async function getAccountInfo() {
  try {
    var res = await Fetch("/accounts", "get", null, true);
    return WrapResult(true, res);
  } catch (ex) {
    return WrapResult(false, ex);
  }
}

export async function withdrawSalary() {
  try {
    var res = await Fetch("/writers/withdraw", "post", null, true);
    return WrapResult(true);
  } catch (ex) {
    return WrapResult(false, null, ex);
  }
}

export async function getFollowingTags() {
  try {
    var res = await Fetch("/auth/accounts/GetFollowingTags", "get", null, true);
    return res;
  } catch (ex) {
    return [];
  }
}

export const getRecommendedWriters = async () => {
  try {
    var res = await Fetch("/auth/accounts/getrecommendedwriters", "get");
    return res;
  } catch {
    return [];
  }
};

export const saveBlog = async (id) => {
  return await Fetch("/auth/accounts/saveblog/" + id, "post", null, true);
};

export const getSavedPosts = async () => {
  try {
    const ids = await Fetch("/auth/accounts/getsavedblogs", "get", null, true);
    const posts = await Fetch("/blog/readers/getsavedblogs", "post", ids, true);
    return posts;
  } catch (e) {
    console.log(e);
    return [];
  }
};
