import Fetch from "../helpers/fetch";

export const GetRecommended = async () => {
  try {
    return await Fetch("/publication/api/publication/getrecommended", "get");
  } catch {
    return [];
  }
};

export const GetUserPubs = async () => {
  try {
    return await Fetch(
      "/publication/api/publication/getuserpublications",
      "get",
      null,
      true
    );
  } catch {
    return [];
  }
};

export const GetFollowingPubs = async () => {
  try {
    return await Fetch(
      "/publication/api/publication/GetFollowingPublishers",
      "get",
      null,
      true
    );
  } catch {
    return [];
  }
};
