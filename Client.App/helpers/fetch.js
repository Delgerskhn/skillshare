import { GetUser } from "./user-store";
import getConfig from "next/config";

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
const API_URI = serverRuntimeConfig.URI || publicRuntimeConfig.URI;

export const WrapResult = (ok, data, message) => {
  return {
    Ok: ok,
    Data: data,
    Message: message,
  };
};

const Fetch = async (path, method, body, secure = false) => {
  var headers = new Headers();
  if (secure) headers.append("Authorization", `Bearer ${GetUser()?.jwtToken}`);
  if (method.toUpperCase() != "GET")
    headers.append("Content-Type", "application/json");

  var options = {
    method: method,
    redirect: "follow",
    headers: headers,
  };
  if (body) options.body = JSON.stringify(body);
  var req = await fetch(`${API_URI}${path}`, options);
  var res = await req.text();
  try {
    var temp = JSON.parse(res);
    res = temp;
  } catch (err) {}
  console.log(req, res);
  if (req.status === 401) window.location.pathname = "/auth/login";
  if (!req.ok) throw res;
  return res;
};

export default Fetch;
