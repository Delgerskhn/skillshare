const monthNames = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export const getMonthName = (date) => {
  return monthNames[date.getMonth()];
};

export const getPostCardDate = (date) => {
  const d = new Date(date);
  return `${getMonthName(d)} ${d.getDate()}`;
};
