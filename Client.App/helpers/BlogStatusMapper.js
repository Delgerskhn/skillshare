export const mapStatus = (i) => {
  if (i == 0) return "PENDING";
  if (i == 1) return "APPROVED";
  if (i == 2) return "DECLINED";
  if (i == 3) return "DRAFT";
};
