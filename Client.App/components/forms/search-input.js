import { IconButton, InputBase, Paper, TextField } from "@material-ui/core";
import { styled } from "@material-ui/styles";
import React, { useState } from "react";
import SearchIcon from "@material-ui/icons/Search";

const SearchBox = styled(InputBase)(() => ({
  "& fieldset": {
    borderRadius: "25px",
  },
}));
export const SearchInput = ({ onSubmitCallback, initial = "" }) => {
  const [value, setValue] = useState(initial);
  const onSubmit = (e) => {
    e.preventDefault();
    onSubmitCallback && onSubmitCallback(value);
  };
  return (
    <div noValidate autoComplete="off" onSubmit={onSubmit}>
      <Paper
        component="form"
        sx={{ p: "2px 4px", display: "flex", alignItems: "center", width: 400 }}
      >
        <IconButton sx={{ p: "10px" }} aria-label="menu">
          <SearchIcon />
        </IconButton>
        <SearchBox
          // style={{ width: "100%" }}
          sx={{ width: "100%" }}
          value={value}
          onChange={(e) => setValue(e.target.value)}
          id="search"
          placeholder="Search content..."
        />
      </Paper>
    </div>
  );
};
