import React, { useEffect, useState } from "react";
import { Post } from "../blogs/post";

export const PostsContainer = ({ fetchPosts }) => {
  const [posts, set_posts] = useState([]);
  useEffect(() => {
    (async () => {
      set_posts(await fetchPosts());
    })();
  }, []);
  return (
    <React.Fragment>
      {posts.map((post) => (
        <Post key={post.title} post={post} />
      ))}
    </React.Fragment>
  );
};
