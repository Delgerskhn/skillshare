import { Grid, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { GetRecommended } from "../../api/publication";
import { PublicationProfile } from "../account/publication.profile";

export const PublicationsContainer = ({ getDataSource, title }) => {
  const [pubs, set_pubs] = useState([]);
  const fetchPubs = async () => {
    set_pubs(await getDataSource());
  };

  useEffect(() => {
    fetchPubs();
    return fetchPubs;
  }, []);

  return (
    <Grid md={12} item>
      <Typography mt={2} variant="subtitle1" color="textSecondary">
        {title}
      </Typography>
      {pubs.map((p) => (
        <PublicationProfile key={p.id} src="" name={pub.name} />
      ))}
    </Grid>
  );
};
