import { Box, Chip, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export const TagsContainer = ({ getDataSource, title }) => {
  const [tags, set_tags] = useState([]);
  const router = useRouter();
  const fetchTags = async () => {
    set_tags(await getDataSource());
  };
  useEffect(() => {
    fetchTags();
    return fetchTags;
  }, []);

  return (
    <>
      <Typography variant="subtitle1" color="textSecondary">
        {title}
      </Typography>
      <Box display="inline-flex" gap={1} flexWrap="wrap" m={2}>
        {tags.map((t) => (
          <Chip
            clickable
            label={t.name}
            key={t.id}
            onClick={() => router.push("/?tag=" + t.id)}
          />
        ))}
      </Box>
    </>
  );
};
