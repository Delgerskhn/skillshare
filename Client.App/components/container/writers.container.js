import { Box, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { WriterProfile } from "../account/writer.profile";

export const WritersContainer = ({ title, getDataSource }) => {
  const [writers, set_writers] = useState([]);

  const fetchWriters = async () => {
    set_writers(await getDataSource());
  };
  useEffect(() => {
    fetchWriters();
    return fetchWriters;
  }, []);

  return (
    <>
      <Typography variant="subtitle1" color="textSecondary">
        {title}
      </Typography>
      <Box display="inline-flex" gap={1} flexWrap="wrap" m={2}>
        {writers.map((r) => (
          <WriterProfile key={r.id} user={r} />
        ))}
      </Box>
    </>
  );
};
