import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Link,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles({
  card: {
    display: "flex",
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
    height: "100%",
  },
});
const post = {
  title: "title",
  created: new Date(),
  description: "new public",
  img: "",
};
export const Overview = () => {
  const classes = useStyles();
  return (
    <Link href={""}>
      <CardActionArea component="a">
        <Card className={classes.card}>
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                {post.title}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {new Date(post.created).toISOString().split("T")[0]}
              </Typography>
              <Box component="div" display={{ xs: "none", md: "block" }}>
                <Typography variant="subtitle1" paragraph>
                  {post.description}
                </Typography>
              </Box>
              <Typography variant="subtitle1" color="primary">
                Continue reading...
              </Typography>
            </CardContent>
          </div>
          <Box
            component="div"
            display={{ xs: "none", md: "block" }}
            p={1}
            m={1}
            bgcolor="background.paper"
          >
            <CardMedia
              className={classes.cardMedia}
              image={post.img}
              title={post.title}
            />
          </Box>
          {/*<Hidden xsDown>
      </Hidden>*/}
        </Card>
      </CardActionArea>
    </Link>
  );
};
