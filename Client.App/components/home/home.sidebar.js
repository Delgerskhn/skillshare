import { Avatar, Box, Chip, Grid, Typography } from "@material-ui/core";
import { GetRecommended } from "../../api/publication";
import { fetchTags, getAllTags, getTags } from "../../api/tags";
import { PublicationProfile } from "../account/publication.profile";
import { WriterProfile } from "../account/writer.profile";
import { SearchInput } from "../forms/search-input";
import { PublicationsContainer } from "../container/recommended.publications";
import { TagsContainer } from "../container/TagsContainer";
import { getFollowingTags, getRecommendedWriters } from "../../api/account";
import { WritersContainer } from "../container/writers.container";
import { useAuth } from "../../context/auth";
import { useRouter } from "next/router";

export const HomeSidebar = ({ searchContent }) => {
  const { user } = useAuth();
  const router = useRouter();
  const { q } = router.query;
  return (
    <Grid item container md={4}>
      <Grid md={12} item>
        <SearchInput initial={q} onSubmitCallback={searchContent} />
      </Grid>
      <PublicationsContainer
        getDataSource={GetRecommended}
        title="PUBLICATIONS"
      />
      <Grid md={12} item>
        <WritersContainer
          title="WRITERS"
          getDataSource={getRecommendedWriters}
        />
      </Grid>
      <Grid md={12} item>
        <TagsContainer title="TAGS" getDataSource={getAllTags} />
      </Grid>
    </Grid>
  );
};
