import { Box, Chip, Grid, Tab, Tabs, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { Post } from "../blogs/post";
import { useRouter } from "next/router";
import { followTag, getTag } from "../../api/tags";
import { getFollowingTags } from "../../api/account";

export const HomePosts = ({ posts }) => {
  const [selected, set_selected] = useState(0);
  const [Tag, set_tag] = useState(null);
  const [is_following, set_is_following] = useState(false);
  const router = useRouter();
  const { tag } = router.query;

  useEffect(() => {
    getTag(tag).then((r) => set_tag(r));
    getFollowingTags().then((r) =>
      set_is_following(r.filter((p) => p.id == tag).length > 0)
    );
  }, []);

  const handleChange = (event, newValue) => {
    set_selected(newValue);
  };

  const onFollow = () => {
    set_is_following(!is_following);
    followTag(Tag.id);
  };

  return (
    <Grid item container spacing={4} md={8}>
      <Grid item md={12}>
        {tag ? (
          <Box
            display="flex"
            alignItems="center"
            sx={{ borderBottom: 1, borderColor: "divider" }}
          >
            <Typography mr={4} variant="h4" color="textSecondary">
              {Tag?.name}
            </Typography>
            <Chip
              onClick={onFollow}
              label={is_following ? "Following" : "Follow"}
              clickable
              variant={is_following ? "filled" : "outlined"}
            />
          </Box>
        ) : (
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={selected}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="secondary"
            >
              <Tab label="Following" />
              <Tab label="Recommended" />
            </Tabs>
          </Box>
        )}
      </Grid>
      {posts.map((post) => (
        <Post key={post.title} post={post} />
      ))}
    </Grid>
  );
};
