import { Box, Typography } from "@material-ui/core";
import BackgroundLetterAvatars from "./avatar";

export const WriterProfile = ({ user }) => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <BackgroundLetterAvatars user={user} />
      {/* <Typography color="textSecondary">
        {user.firstName || user.email}
      </Typography> */}
    </Box>
  );
};
