import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Avatar,
  Box,
  Button,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import StarIcon from "@material-ui/icons/Star";
import BackgroundLetterAvatars from "./avatar";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { useAuth } from "../../context/auth";

const styles = {};

const useStyles = makeStyles(styles);
export default function AppProfile({}) {
  const { user, signOut } = useAuth();
  const classes = useStyles();
  return (
    <Box
      display="flex"
      justifyContent="flex-start"
      direction="row"
      alignItems="center"
    >
      <Box alignItems="center" display="flex">
        <Box mr={2}>
          <BackgroundLetterAvatars user={user} />
        </Box>
        <Typography variant="subtitle1" color="textSecondary">
          {user.firstName || user.email}
        </Typography>
        <IconButton alignItems="center" display="flex" onClick={signOut}>
          <ExitToAppIcon />
        </IconButton>
      </Box>
    </Box>
  );
}
