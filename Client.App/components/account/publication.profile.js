import { Avatar, Box, Button, Grid, Typography } from "@material-ui/core";

export const PublicationProfile = ({ name, src }) => {
  return (
    <Button>
      <Box mx={2} my={1} display="inline-flex" gap={2} alignItems="center">
        <Avatar sx={{ width: 30, height: 30 }} alt={name} src={src} />
        <Typography color="black">{name}</Typography>
      </Box>
    </Button>
  );
};
