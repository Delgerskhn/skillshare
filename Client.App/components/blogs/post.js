import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Box from "@material-ui/core/Box";
import Link from "next/link";
import { constBlog } from "../../shared/constants";
import PostController from "./post-controller";
import { Avatar, Chip, IconButton } from "@material-ui/core";
import { getPostCardDate } from "../../helpers/date";
import { mapStatus } from "../../helpers/BlogStatusMapper";
import TurnedInNotIcon from "@material-ui/icons/TurnedInNot";
import { saveBlog } from "../../api/account";

const useStyles = makeStyles({
  card: {
    display: "flex",
    flexDirection: "column",
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
    height: "100%",
  },
});

export function Post(props) {
  const classes = useStyles();
  const { post, hasController } = props;
  const getJumpUrl = () => {
    if (post?.blogStatus === "APPROVED") return "/blog?pk=" + post.id;
    return "/editor?pk=" + post.id;
  };

  const onSave = (e) => {
    e.stopPropagation();
    saveBlog(post.id);
  };

  return (
    <Grid item xs={12}>
      <Link href={getJumpUrl()}>
        <CardActionArea component="a">
          <Card className={classes.card}>
            <Box
              mx={2}
              mt={2}
              display="inline-flex"
              gap={2}
              alignItems="center"
            >
              <Avatar
                sx={{ width: 30, height: 30 }}
                alt={post?.user?.userName || post?.email}
                src={post.imgUrl}
              />
              <Typography color="black">
                {post?.user?.userName || post?.email}
              </Typography>
              <Typography color="textSecondary">
                {getPostCardDate(post.created)}
              </Typography>
            </Box>
            <Box display="flex" justifyContent="space-between">
              <div className={classes.cardDetails}>
                <CardContent>
                  <Typography fontWeight="bold" component="h2" variant="h5">
                    {post.title}
                  </Typography>

                  <Box component="div" display={{ xs: "none", md: "block" }}>
                    <Typography variant="subtitle1" paragraph>
                      {post.description}
                    </Typography>
                  </Box>
                </CardContent>
              </div>
              <Box
                component="div"
                display={{ xs: "none", md: "block" }}
                p={1}
                m={1}
                bgcolor="background.paper"
              >
                <CardMedia
                  className={classes.cardMedia}
                  image={post.img}
                  title={post.title}
                />
              </Box>
            </Box>
            <Box
              mx={2}
              mb={2}
              display="inline-flex"
              gap={2}
              alignItems="center"
            >
              <Chip size="sm" label={post.tags[0]?.name} />
              <Typography color="textSecondary">2min read</Typography>

              <Box>
                <IconButton size="sm" onClick={onSave}>
                  <TurnedInNotIcon color="textSecondary" />
                </IconButton>
              </Box>
            </Box>
            {/*<Hidden xsDown>
          </Hidden>*/}
          </Card>
        </CardActionArea>
      </Link>
      <PostController visible={hasController} post={post} />
    </Grid>
  );
}

Post.propTypes = {
  post: PropTypes.object,
};
