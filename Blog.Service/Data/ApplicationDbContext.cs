using Blog.Service.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;
using BlogPost = Blog.Service.Entities.Blog;

namespace Blog.Service.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // Get DbString from docker env
            // var connStr = Configuration["DbString"] ?? Configuration.GetConnectionString("Database");
            // options.UseNpgsql(connStr, x => x.MigrationsAssembly("Migrations"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // builder
            //     .buildAccount()
            //     .buildOrder()
            //     .buildTransaction()
            //     .buildProduct();
            builder.Entity<BlogPost>().Property(r => r.BlogStatus).HasColumnType("varchar(20)");
            builder.Entity<BlogPost>().HasOne(r => r.User);
            base.OnModelCreating(builder);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            var utcNow = DateTime.Now;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                if (entry.Entity is BaseEntity trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            // set the updated date to "now"
                            trackable.Updated = utcNow;

                            // mark property as "don't touch"
                            // we don't want to update on a Modify operation
                            entry.Property("Created").IsModified = false;
                            break;

                        case EntityState.Added:
                            // set both updated and created date to "now"
                            trackable.Created = utcNow;
                            trackable.Updated = utcNow;
                            break;
                    }
                }
            }
        }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<BlogPost> Blogs { get; set; }
        public DbSet<Like> Likes { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<View> Views { get; set; }
        public virtual DbSet<ReadList> ReadLists { get; set; }
        //public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        //public virtual DbSet<User> Users { get; set; }
    }
}