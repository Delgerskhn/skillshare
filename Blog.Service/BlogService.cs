using Blog.Service.Data;
using Blog.Service.Entities;
using Blog.Service.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogPost = Blog.Service.Entities.Blog;

namespace Blog.Service
{
    public interface IBlogService
    {
        Task<BlogPost> GetUserBlog(int pk, int userpk);
        Task<List<BlogPost>> GetUserBlogsByStatus(int userpk, BlogStatus status);
        Task UpdateUserBlog(int userpk, BlogPost blog);
        Task CreateUserBlog(int userpk, BlogPost blog);
        Task DeleteUserBlog(int pk, int userpk);
        Task<List<BlogPost>> GetBlogsByStatus(BlogStatus statusPk);
        bool UserBlogExists(int pk, int userpk);
        Task<BlogPost> GetBlog(int pk);
        Task UpdateBlogStatus(BlogPost blog);
        Task<List<BlogPost>> GetBlogsByTag(Tag tag);
        Task<List<BlogPost>> GetLatestBlogs();
        Task<List<BlogPost>> GetBlogsByTags(int tagPk);
        Task UpdateBlogViews(BlogPost blog, int? uid);
    }
    public class BlogService : IBlogService
    {
        private readonly ApplicationDbContext _context;

        public BlogService(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool UserBlogExists(int pk, int userpk)
        {
            return _context.Blogs.Any(e => e.Id == pk && e.UserId == userpk);
        }

        public async Task CreateUserBlog(int userpk, BlogPost blog)
        {
            blog.UserId = userpk;
            blog.BlogStatus = BlogStatus.DRAFT;
            _context.AttachRange(blog.Tags);
            _context.Blogs.Add(blog);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUserBlog(int pk, int userpk)
        {
            _context.Blogs.Remove(await GetUserBlog(pk, userpk));
            await _context.SaveChangesAsync();
        }

        public async Task<BlogPost> GetBlog(int pk)
        {
            return await _context.Blogs.Where(r => r.Id == pk)
                            .Include(r => r.Tags)
                            .Include(r => r.User)
                            .Include(r => r.Comments).FirstAsync();
        }

        public async Task<List<BlogPost>> GetBlogsByStatus(BlogStatus statusPk)
        {
            return await _context.Blogs
                           .Where(r => r.BlogStatus == statusPk)
                           .Include(r => r.User)
                           .ToListAsync();
        }

        public async Task<List<BlogPost>> GetBlogsByTag(Tag tag)
        {
            var q = await (from b in _context.Blogs.Where(r => r.BlogStatus == BlogStatus.APPROVED)
                           where b.Tags.Contains(tag)
                           select new BlogPost
                           {
                               Id = b.Id,
                               Title = b.Title,
                               Img = b.Img,
                               Content = b.Content,
                               User = new Account
                               {
                                   UserName = b.User.UserName
                               },
                               Tags = b.Tags,
                               Created = b.Created
                           }).ToListAsync();
            return q;
        }

        public async Task<List<BlogPost>> GetLatestBlogs()
        {
            return await _context.Blogs
                          .Where(r => r.BlogStatus == BlogStatus.APPROVED)
                          .OrderByDescending(r => r.Created)
                          .Include(r => r.User).Include(r => r.Tags).Take(Constants.Blogs.PagingSize)
                          .ToListAsync();
        }

        public async Task<BlogPost> GetUserBlog(int pk, int userpk)
        {
            var blog = await _context.Blogs.Where(r => r.Id == pk
            && r.UserId == userpk
            ).Include(r => r.Tags).FirstOrDefaultAsync();
            return blog;
        }

        public async Task<List<BlogPost>> GetUserBlogsByStatus(int userpk, BlogStatus status)
        {
            var q = await _context.Blogs.Where(r => r.BlogStatus == status && r.UserId == userpk).ToListAsync();
            // var q = await _context.BlogStatuses.Where(r => r.Pk == status)
            //         .Include(r => r.Blogs.Where(b => b.UserPk == userpk))
            //         .Select(r => r.Blogs.ToList())
            //         .FirstOrDefaultAsync();
            return q;
        }

        public async Task UpdateBlogStatus(BlogPost blog)
        {
            _context.Blogs.Update(blog);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateUserBlog(int userpk, BlogPost blog)
        {
            var persistentBlog = await _context.Blogs.Where(r => r.Id == blog.Id).Include(r => r.Tags).FirstAsync();
            persistentBlog.BlogStatus = BlogStatus.DRAFT;
            persistentBlog.Img = blog.Img;
            persistentBlog.Title = blog.Title;
            persistentBlog.Content = blog.Content;
            persistentBlog.Description = blog.Description;

            var tagsToRemove = persistentBlog.Tags.LeftExcept(blog.Tags);
            foreach (var t in tagsToRemove) persistentBlog.Tags.Remove(t);
            var tagsToAdd = blog.Tags.LeftExcept(persistentBlog.Tags);
            foreach (var t in tagsToAdd) persistentBlog.Tags.Add(t);

            await _context.Database.ExecuteSqlRawAsync(string.Format("SELECT public.upd_blog_vectors({0})", blog.Id));

            await _context.SaveChangesAsync();
        }

        public async Task<List<BlogPost>> GetBlogsByTags(int tagPk)
        {
            var t = _context.Tags.Find(tagPk);
            var q = await _context.Blogs.Where(r => r.Tags.Contains(t)).OrderByDescending(r => r.Created).ToListAsync();
            // var q = await _context.Tags.Where(r => r.Id == tagPk).Include(r => r.Blogs).Select(r => r.Blogs).ToListAsync();
            return q;
        }

        public async Task UpdateBlogViews(BlogPost blog, int? uid)
        {
            var v = new View()
            {
                Blog = blog,
                AccountId = uid,
                Created = DateTime.Now
            };
            await _context.Views.AddAsync(v);
            await _context.SaveChangesAsync();
        }
    }
}