﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

#nullable disable

namespace Blog.Service.Entities
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BlogStatus
    {
        PENDING,
        APPROVED,
        DECLINED,
        REVIEWED,
        REMOVED,
        DRAFT
    }
}
