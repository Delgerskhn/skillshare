namespace Blog.Service.Entities
{
    public class View : BaseEntity
    {
        public int? AccountId { get; set; }
        public int BlogId { get; set; }

        public Blog Blog { get; set; }

        public Account Account { get; set; }

    }
}