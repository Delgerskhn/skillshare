﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Blog.Service.Entities
{
    public partial class ReadList : BaseEntity
    {
        public int BlogId { get; set; }

        public virtual Blog Blog { get; set; }
        public virtual Account User { get; set; }
    }
}
