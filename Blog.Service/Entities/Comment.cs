﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Blog.Service.Entities
{
    public partial class Comment : BaseEntity
    {
        public int? BlogId { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }

        public virtual Blog Blog { get; set; }
        public virtual Account User { get; set; }
    }
}
