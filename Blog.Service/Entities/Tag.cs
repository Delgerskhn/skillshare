﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

#nullable disable

namespace Blog.Service.Entities
{
    public partial class Tag : BaseEntity
    {
        public Tag()
        {
        }
        public string Name { get; set; }

        public ICollection<Blog> Blogs { get; set; }
    }
}
