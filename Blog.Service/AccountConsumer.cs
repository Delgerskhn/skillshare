
using System.Threading.Tasks;
using Blog.Service.Data;
using Blog.Service.Entities;
using MassTransit;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Shared.Events.Commands;

namespace Blog.Service
{

    class AccountConsumer :
            IConsumer<CreateAccountCommand>
    {
        ILogger<AccountConsumer> _logger;
        private readonly ApplicationDbContext _context;

        public AccountConsumer(ILogger<AccountConsumer> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Consume(ConsumeContext<CreateAccountCommand> context)
        {
            _logger.LogInformation("Value: {Value}", context.Message);
            var account = JsonConvert.DeserializeObject<Account>(JsonConvert.SerializeObject(context.Message));
            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();
        }
    }
}