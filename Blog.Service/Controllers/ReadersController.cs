﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blog.Service.Data;
using Blog.Service.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogPost = Blog.Service.Entities.Blog;

namespace Blog.Service.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ReadersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IBlogService _blogService;
        public ReadersController(ApplicationDbContext context, IBlogService blogService)
        {
            _context = context;
            _blogService = blogService;
        }

        //TODO: can have multiple tags 
        [HttpGet("tag/{tagPk}")]
        public async Task<ActionResult> GetBlogsByTag(int tagPk)
        {
            var blogs = await _blogService.GetBlogsByTags(tagPk);
            return Ok(blogs);
        }

        [HttpPost("search")]
        public async Task<ActionResult<IEnumerable<BlogPost>>> GetBlogsByKeyWords([FromBody] string query)
        {
            var q = await _context.Blogs.FromSqlRaw("select * from Blogs_Sel_Query({0})", query).ToListAsync();
            return q;
        }

        [HttpGet("latest")]
        public async Task<ActionResult<IEnumerable<BlogPost>>> GetLatestBlogs()
        {
            return await _blogService.GetLatestBlogs();

        }

        // GET: api/Readers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlogPost>> GetBlog(int id, int? userId)
        {
            var blog = await _blogService.GetBlog(id);

            if (blog == null || blog.BlogStatus != BlogStatus.APPROVED)
            {
                return NotFound();
            }
            await _blogService.UpdateBlogViews(blog, userId);
            return blog;
        }

        [HttpPost("like")]
        public async Task<ActionResult> LikeBlog([FromBody] Like like)
        {
            if (!BlogExists((int)like.BlogId)) return BadRequest("The blog identity is wrong!");
            await _context.Likes.AddAsync(like);
            var blog = await _blogService.GetBlog(like.BlogId);
            blog.Likes++;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpPost("comment")]
        public async Task<ActionResult> CommentBlog([FromBody] Comment comment)
        {
            if (!BlogExists((int)comment.BlogId)) return BadRequest("The blog identity is wrong!");
            await _context.AddAsync(comment);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpGet("tags")]
        public async Task<IActionResult> GetTags()
        {
            return Ok(await _context.Tags.ToListAsync());
        }

        [HttpGet("gettag/{id}")]
        public async Task<IActionResult> GetTag(int id)
        {
            if (!_context.Tags.Any(r => r.Id == id)) return BadRequest("Tag doesn't exist");
            return Ok(await _context.Tags.FindAsync(id));
        }

        [HttpPost("getsavedblogs")]
        public IActionResult GetSavedblogs([FromBody] List<int> blogIds)
        {
            return Ok(_context.Blogs.Where(r => blogIds.Contains(r.Id)).ToList());
        }


        private bool BlogExists(int blogPk)
        {
            return _context.Blogs.Find(blogPk) != null;
        }

    }
}