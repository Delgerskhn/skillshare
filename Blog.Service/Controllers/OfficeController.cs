﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Blog.Service.Data;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
using BlogPost = Blog.Service.Entities.Blog;
using Blog.Service.Entities;

namespace Blog.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IBlogService _blogService;

        public OfficeController(ApplicationDbContext context, IBlogService blogService)
        {
            _context = context;
            _blogService = blogService;
        }

        [HttpGet("blogs/status/{status}")]
        public async Task<ActionResult<IEnumerable<BlogPost>>> GetBlogsByStatus(BlogStatus status)
        {
            return Ok(await _blogService.GetBlogsByStatus(status));
        }
        [HttpGet("blogs/{pk}")]
        public async Task<ActionResult> GetBlog(int pk)
        {
            var blog = await _blogService.GetBlog(pk);
            if (blog == null) return NotFound("Blog doesn't exist");
            return Ok(blog);
        }

        [HttpPost("blogs/change")]
        public async Task<ActionResult> Post([FromQuery] int blogPk, [FromQuery] BlogStatus statusPk)
        {
            var blog = await _blogService.GetBlog(blogPk);
            if (blog == null) return NotFound("Blog doesn't exist");
            blog.BlogStatus = statusPk;
            await _blogService.UpdateBlogStatus(blog);
            return NoContent();
        }

        [HttpGet("blogs/views-by-week")]
        public async Task<ActionResult> ViewsByWeek()
        {
            var views = await _context.Views
            .Where(r => r.Created > DateTime.Now.AddDays(-7))
            .GroupBy(r => r.Created.DayOfWeek).Select(r => new
            {
                Day = r.Key,
                Views = r.Count()
            }).ToListAsync();
            return Ok(views);
        }



    }
}
