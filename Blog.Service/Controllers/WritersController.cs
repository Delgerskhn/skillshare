﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blog.Service.Data;
using Blog.Service.Entities;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.Events.Commands;
using BlogPost = Blog.Service.Entities.Blog;
namespace Blog.Service.Controllers
{
    [Route("[controller]")]
    [Authorize("User", "Admin")]
    [ApiController]
    public class WritersController : ControllerBase
    {
        private readonly IBlogService _blogService;
        private readonly ClaimsPrincipal _caller;
        private readonly ApplicationDbContext _context;
        private readonly IPublishEndpoint _publishEndpoint;


        public WritersController(
            IBlogService blogService,
            IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext context
, IPublishEndpoint publishEndpoint)
        {
            _caller = httpContextAccessor.HttpContext.User;
            _blogService = blogService;
            _context = context;
            _publishEndpoint = publishEndpoint;
        }

        [HttpGet("/writer/userid")]
        public int GetUserId()
        {
            return Int32.Parse(_caller.Claims.Single(c => c.Type == "id").Value);
        }

        [HttpPost("publish/{pk}")]
        public async Task<IActionResult> Publish(int pk)
        {
            if (_blogService.UserBlogExists(pk, GetUserId()))
            {
                var blog = await _blogService.GetUserBlog(pk, GetUserId());
                blog.BlogStatus = BlogStatus.PENDING;
                await _blogService.UpdateBlogStatus(blog);
                return NoContent();
            }
            else
            {
                return BadRequest("Blog doesn't exist!");
            }
        }

        // GET: api/Writers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BlogPost>>> GetBlogs([FromQuery] BlogStatus Status)
        {
            return Ok(await _blogService.GetUserBlogsByStatus(GetUserId(), Status));
        }

        // GET: api/Writers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlogPost>> GetBlog(int id)
        {
            var blog = await _blogService.GetUserBlog(id, GetUserId());

            if (blog == null)
            {
                return NotFound();
            }

            return blog;
        }

        // PUT: api/Writers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBlog(int id, BlogPost blog)
        {
            if (id != blog.Id)
            {
                return BadRequest("Blog must contain the identity");
            }
            if (!_blogService.UserBlogExists(id, GetUserId()))
            {
                return NotFound();
            }
            await _blogService.UpdateUserBlog(GetUserId(), blog);
            return NoContent();
        }

        // POST: api/Writers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BlogPost>> PostBlog(BlogPost blog)
        {
            await _blogService.CreateUserBlog(GetUserId(), blog);
            return CreatedAtAction("GetBlog", new { id = blog.Id }, blog);
        }

        [HttpPost("tag")]
        public async Task<ActionResult> PostTag(Tag tag)
        {
            if (!(await _context.Tags.AnyAsync(r => r.Id == (int)tag.Id || r.Name == tag.Name)))
            {
                await _context.AddAsync(tag);
                await _context.SaveChangesAsync();
                await _publishEndpoint.Publish<CreateTagCommand>(tag);
                return new CreatedResult("Tag", tag);
            }
            return new BadRequestObjectResult("Tag already exist!");
        }



        // DELETE: api/Writers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            if (!_blogService.UserBlogExists(id, GetUserId()))
            {
                return NotFound();
            }

            await _blogService.DeleteUserBlog(id, GetUserId());

            return NoContent();
        }


    }
}
