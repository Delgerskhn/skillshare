﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Blog.Service.Migrations
{
    public partial class views_nullable_acc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Views_Accounts_AccountId",
                table: "Views");

            migrationBuilder.AlterColumn<int>(
                name: "AccountId",
                table: "Views",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Views_Accounts_AccountId",
                table: "Views",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Views_Accounts_AccountId",
                table: "Views");

            migrationBuilder.AlterColumn<int>(
                name: "AccountId",
                table: "Views",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Views_Accounts_AccountId",
                table: "Views",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
