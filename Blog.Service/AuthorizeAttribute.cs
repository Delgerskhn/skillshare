using Blog.Service.Data;
using Blog.Service.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    private readonly IList<string> _roles;

    public AuthorizeAttribute(params string[] roles)
    {
        _roles = roles ?? new string[] { };
    }

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        try
        {
            var id = context.HttpContext.User.Claims.Single(c => c.Type == "id").Value;
            var role = context.HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.Role).Value;
            if (id == null || (_roles.Any() && !_roles.Contains(role)))
            {
                throw new Exception();
            }
        }
        catch
        {
            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };

        }

    }
}