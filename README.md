# Microservice based blog website

### Goal

This document covers the insights of blog website architecture, design and implementation.

### Technologies used

1.  .NET 5.0, C#
2.  RabbitMQ
3.  ReactJS
4.  NextJS
5.  Docker
6.  PostgreSQL

### Application startup instruction

1. Clone this repository into your machine.
2. Docker must be installed on your machine. https://www.docker.com/
3. Using CD command in shell navigate to the root folder of the repository.
4. Run "docker compose up -d" to start the services.
5. Client and Admin applications will be listening on ports 3000, 3001.

# System analysis

The business logic of this website is more likely to be managed by certain type of organization. The organization staffs, or admin is able to approve or decline the blog posts written by a blogger or a group of bloggers.

## Business activity diagram

The general operation of the system is illustrated below. The blog post can be validated up to 2 times before it reaches the reader. Because the system user can be a publication admin himself, the system staff has to re-verify the blog approved by the publication admin.
![Activity diagram](Img/Screenshot_5.png)

## Use case diagram

Based on customer requirements, the system can be operated from each user's point of view. Abstract identification of activities are shown in the diagram below.
![Use case diagram](Img/Screenshot_6.png)

# System design

Now let's consider what technical models used to design this system.

## General system architecture

The system is composed of 2 client application and 5 back-end services. Additionally the system uses an API gateway and a message broker.
![System architecture](Img/Screenshot_7.png)

## Architecture of front-end applications

Both client and admin application uses same pattern. Components are responsible for rendering changed information. The contexts contain core business logics of the application and calls API functions to send HTTP request to the server.
![Front-end application architecture](Img/Screenshot_8.png)

## Entity relationship diagram

When using microservices, all services must be independent from each other. Thus some data can be duplicated in different database.

### Blog service ERD

![Blog service ERD](Img/blogserviceerd.png)

### Authentication service ERD

![Authentication service ERD](Img/auth.service.erd.png)

### Publication service ERD

![Publication service ERD](Img/publication.service.erd.png)

### Notification service ERD

![Notification service ERD](Img/notification.service.erd.png)
