export const constEditor = {
  AutoSaveDuration: 2,
  MsPerSecond: 1000,
  InitialValue: [
    {
      type: "heading-one",
      children: [{ text: "" }],
    },
  ],
};

export const constBlog = {
  State: {
    Pending: "PENDING",
    Published: "APPROVED",
    Declined: "DECLINED",
    Draft: "DRAFT",
  },
};
