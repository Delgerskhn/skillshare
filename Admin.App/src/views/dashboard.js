import React, { useEffect, useState } from "react";

import { Chart, Series } from "devextreme-react/chart";

import PieChart, {
  Series as PieSeries,
  Label,
  Connector,
  Size,
  Export,
} from "devextreme-react/pie-chart";
import { getViewsByWeek } from "../api/blogs";
export const Dashboard = () => {
  const [weeklyViews, setWeeklyViews] = useState(dataSource);
  useEffect(() => {
    (async () => {
      var views = await getViewsByWeek();
      let ds = [...dataSource];
      for (let a of views) {
        let { day, views } = a;
        ds[day - 1].oranges = views;
      }
      setWeeklyViews(ds);
    })();
  }, []);
  return (
    <React.Fragment>
      <h2 className={"content-block"}>Weekly views</h2>
      <Chart id="chart" dataSource={weeklyViews}>
        <Series
          valueField="oranges"
          argumentField="day"
          name="My oranges"
          type="bar"
          color="#ffaa66"
        />
      </Chart>

      <PieChart
        id="pie"
        dataSource={areas}
        palette="Bright"
        title="Views by tags"
      >
        <PieSeries argumentField="country" valueField="area">
          <Label visible={true}>
            <Connector visible={true} width={1} />
          </Label>
        </PieSeries>

        <Size width={500} />
        <Export enabled={true} />
      </PieChart>
    </React.Fragment>
  );
};

export const areas = [
  {
    country: "Kitchen",
    area: 12,
  },
  {
    country: "Laptop",
    area: 7,
  },
  {
    country: "Productivity",
    area: 7,
  },
  {
    country: "Programming",
    area: 7,
  },
];

export const dataSource = [
  {
    day: "Monday",
    oranges: 0,
  },
  {
    day: "Tuesday",
    oranges: 0,
  },
  {
    day: "Wednesday",
    oranges: 0,
  },
  {
    day: "Thursday",
    oranges: 0,
  },
  {
    day: "Friday",
    oranges: 0,
  },
  {
    day: "Saturday",
    oranges: 0,
  },
  {
    day: "Sunday",
    oranges: 0,
  },
];
