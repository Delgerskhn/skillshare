import React, { useEffect, useState } from "react";
import { Button } from "devextreme-react/button";

import Box, { Item } from "devextreme-react/box";

import BlogEditor from "../components/editor/blog-editor.js";
import { useBlogs } from "../contexts/blog.js";
import { constBlog } from "../utils/constants.js";

export const PublicationDV = ({ data }) => {
  let { user, content, id } = data.data;

  return (
    <React.Fragment>
      <div className="master-detail-caption">{`${user?.email}'s blog`}</div>
      {/* <Button
        text="Approve"
        type="default"
        stylingMode="contained"
        onClick={onApprove}
      />
      <Button
        text="Decline"
        type="default"
        stylingMode="contained"
        onClick={onDecline}
      />
      <BlogEditor readOnly={true} content={parseContent()} /> */}
    </React.Fragment>
  );
};
