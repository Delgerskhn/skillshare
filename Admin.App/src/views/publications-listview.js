import React, { useEffect, useState } from "react";
import { useHistory, withRouter } from "react-router-dom";

import DataGrid, { Column, MasterDetail } from "devextreme-react/data-grid";

import BlogsDV from "./blogs-detailview.js";
import getParam from "../utils/query-string.js";
import { usePublication } from "../contexts/publication.js";
import { PublicationDV } from "./publications-detailview.js";

function imgRender(data) {
  return <img width="150" src={data.value} />;
}

function PublicationsLV(props) {
  const { publications } = usePublication();

  return (
    <DataGrid
      id="grid-container"
      dataSource={publications}
      keyExpr="id"
      columnAutoWidth={true}
      filter
      wordWrapEnabled
      showBorders={true}
    >
      <MasterDetail enabled={true} component={PublicationDV} />
      {/* <Column dataField="title" caption="Title" />
      <Column
        caption="Image"
        dataField="img"
        allowSorting={false}
        cellRender={imgRender}
      />
      <Column dataField="description" caption="Description" width="300" />
      <Column dataField="likes" caption="Likes" dataType="number" />
      <Column dataField="created" caption="Created" dataType="date" />
      <Column dataField="user.userName" caption="Writer" />
      <Column dataField="user.email" caption="Writer email" />
      <MasterDetail enabled={true} component={BlogsDV} /> */}
    </DataGrid>
  );
}

export default withRouter(PublicationsLV);
