import Fetch from "./fetch";

export const getPublications = async () => {
  try {
    var res = await Fetch(
      "/publication/api/publication/getall",
      "get",
      null,
      true
    );
    return res;
  } catch (e) {
    return [];
  }
};
