import React, {
  useState,
  useEffect,
  createContext,
  useContext,
  useCallback,
} from "react";
import CustomStore from "devextreme/data/custom_store";
import { getBlogsByStatus, updateBlogStatus } from "../api/blogs";
import { getPublications } from "../api/publications";

function PublicationProvider(props) {
  const [publications, set_publications] = useState([]);
  const fetch_pubs = async () => {
    set_publications(await getPublications());
  };
  useEffect(() => {
    fetch_pubs();
  }, []);

  return <PublicationContext.Provider value={{ publications }} {...props} />;
}

const PublicationContext = createContext({});
const usePublication = () => useContext(PublicationContext);

export { PublicationProvider, usePublication };
