import React from "react";
import AuthorsListview from "../../views/authors-listview";
import { Dashboard } from "../../views/dashboard";
import "./home.scss";

export default () => (
  <React.Fragment>
    <h2 className={"content-block"}>Home</h2>
    <div className={"content-block"}>
      <Dashboard />
    </div>
  </React.Fragment>
);
