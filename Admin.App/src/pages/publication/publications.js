import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";
import { BlogsProvider } from "../../contexts/blog";
import { PublicationProvider } from "../../contexts/publication";
import { constBlog } from "../../utils/constants";
import getParam from "../../utils/query-string";
import BlogsLV from "../../views/blogs-listview";
import PublicationsListview from "../../views/publications-listview";

function Publications(props) {
  return (
    <React.Fragment>
      <PublicationProvider>
        <h2 className={"content-block"}>Publications</h2>
        <div className={"content-block"}>
          <div className={"dx-card responsive-paddings"}>
            <PublicationsListview />
          </div>
        </div>
      </PublicationProvider>
    </React.Fragment>
  );
}
export default withRouter(Publications);
