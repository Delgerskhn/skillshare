export const navigation = [
  {
    text: "Home",
    path: "/home",
    icon: "home",
  },
  {
    text: "Blogs",
    items: [
      {
        text: "Pending",
        path: "/blogs?status=PENDING",
      },
      {
        text: "Declined",
        path: "/blogs?status=DECLINED",
      },
      {
        text: "Approved",
        path: "/blogs?status=APPROVED",
      },
    ],
    icon: "contains",
  },
  // {
  //   text: "Publications",
  //   path: "/publications",
  //   icon: "contains",
  // },
  // {
  //   text: "Tags",
  //   path: "/tags",
  //   icon: "contains",
  // },
  // {
  //   text: "Writers",
  //   path: "/writers",
  //   icon: "contains",
  // },
];
